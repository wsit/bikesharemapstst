
<?php

require("config.php");
require("db.class.php");
require("actions-web.php");

$db=new Database($dbserver,$dbuser,$dbpassword,$dbname);
$db->connect();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?= $systemname; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="js/viewportDetect.js"></script>
<script type="text/javascript" src="js/leaflet.js"></script>
<script type="text/javascript" src="js/L.Control.Sidebar.js"></script>
<script type="text/javascript" src="js/translations.php"></script>
<script type="text/javascript" src="js/functions.js"></script>
<?php
if (isset($geojson))
   {
   foreach($geojson as $url)
      {
      echo '<link rel="points" type="application/json" href="',$url,'">'."\n";
      }
   }
?>
<?php if (date("m-d")=="04-01") echo '<script type="text/javascript" src="http://maps.stamen.com/js/tile.stamen.js?v1.3.0"></script>'; ?>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrapValidator.min.css" />
<link rel="stylesheet" type="text/css" href="css/leaflet.css" />
<link rel="stylesheet" type="text/css" href="css/L.Control.Sidebar.css" />
<link rel="stylesheet" type="text/css" href="css/map.css" />
<script>
var maplat=<?php echo $systemlat; ?>;
var maplon=<?php echo $systemlong; ?>;
var mapzoom=<?php echo $systemzoom; ?>;
var standselected=0;
<?php
if (isloggedin())
   {
   echo 'var loggedin=1;',"\n";
   echo 'var priv=',getprivileges($_COOKIE["loguserid"]),";\n";
   }
else
   {
   echo 'var loggedin=0;',"\n";
   echo 'var priv=0;',"\n";
   }
if (iscreditenabled())
   {
   echo 'var creditsystem=1;',"\n";
   }
else
   {
   echo 'var creditsystem=0;',"\n";
   }
if (issmssystemenabled()==TRUE)
   {
   echo 'var sms=1;',"\n";
   }
else
   {
   echo 'var sms=0;',"\n";
   }
?>
</script>
<?php if (file_exists("analytics.php")) require("analytics.php"); ?>
</head>
<body>
<div id="map"></div>
<div id="sidebar"><div id="overlay"></div>
<!--modal window-->
<div class="modal" id="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Are you sure?</h5>
      </div>
      <div class="modal-body">
        <p>You WILL be billed if this unit is not RENTED <ins>AND</ins> RETURNED properly</p>
      </div>
      <div class="modal-footer">
        <button type="button" id="cancel" class="btn btn-danger pull pull-left" data-dismiss="modal">Cancel</button>
        <button type="button" id="confirm" class="btn btn-primary pull pull-right">Confirm</button>
      </div>
    </div>
  </div>
</div>
<!--end of modal window -->
<div class="row">
   <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
   <ul class="list-inline">
      <li><a href="#" class="togglehelp"><span class="glyphicon glyphicon-question-sign"></span> <?php echo _('Help'); ?></a></li>
      
<?php
if (isloggedin() AND getprivileges($_COOKIE["loguserid"])>0) echo '<li><a href="admin.php"><span class="glyphicon glyphicon-cog"></span> ',_('Admin'),'</a></li>';
if (isloggedin())
   {
   echo '<li><a class="toggleprofile"><span class="glyphicon glyphicon-user"></span> <small>',getusername($_COOKIE["loguserid"]),'</small></a>';
   if (iscreditenabled()) echo ' (<span id="usercredit" title="',_('Remaining credit'),'">',getusercredit($_COOKIE["loguserid"]),'</span> ',getcreditcurrency(),' <button type="button" class="btn btn-success btn-xs" id="opencredit" title="',_('Add credit'),'"><span class="glyphicon glyphicon-plus"></span></button>)<span id="couponblock"><br /><span class="form-inline"><input type="text" class="form-control input-sm" id="coupon" placeholder="XXXXXX" /><button type="button" class="btn btn-primary btn-sm" id="validatecoupon" title="',_('Confirm coupon'),'"><span class="glyphicon glyphicon-plus"></span></button></span></span></li>';
   echo '<li><a href="command.php?action=logout" id="logout"><span class="glyphicon glyphicon-log-out"></span> ',_('Log out'),'</a></li>';
   }
?>
   </ul>
   </div>
   <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
   </div>
</div>
<div class="row">
   <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
   <table class="table">
   <tr>
    <td><img class="pull-left" src="<?php echo $logourl;?>" style="border-radius: 1%;" width="120" height="100"></td> <td><h1><?php echo _(" ").$systemname; ?></h1></td>
   </tr>
   </table>
   </div>
   <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
   </div>
</div>

<div id="flash"></div>
<!--inquiries-->
<div class="row">
  <form class="container" id="report">
    <h2 id="reporttitle"><?php echo _('Online help'); ?></h2>
    <h4 id="hint"><?php echo _('How can we help you?'); ?></h4>
    <div class="form-group">
         <textarea cols="30" rows="5" name="inquiry" id="inquiry" class="form-control" ></textarea> </div>
    <?php if(isloggedin()): ?>
    <input type="hidden" name="userid" id="userid"  value="<?php echo $_COOKIE['loguserid']; ?>" />
    <?php endif; ?>
    <?php if(!isloggedin()): ?>
    <div class="form-group">
        <label for="phonenumber"><?php echo _('Phone number:'); ?></label> <input type="text" name="phonenumber" id="phonenumber" class="form-control"  placeholder="<?php echo _('5551234567') ; ?>"/></div>
    <div class="form-group">
        <label for="useremail"><?php echo _('Email:'); ?></label> <input type="text" name="useremail" id="useremail" class="form-control"  placeholder="<?php echo _('example@domain.com') ?>" /></div>
    <?php endif; ?>
  
   <button type="submit" id="sendinquiry" class="btn btn-primary" ><?php echo _('Send report'); ?></button>
   <a href="<?php echo $systemrules; ?>"  class="btn" ><?php echo _('Liability'); ?></a>

  </form>
</div>

<?php if (!isloggedin()): ?>
<div id="loginform">
<h1>Log in</h1>
<?php
if (isset($_GET["error"]) AND $_GET["error"]==1) echo '<div class="alert alert-danger" role="alert"><h3>',_('User / phone number or password incorrect! Please, try again.'),'</h3></div>';
elseif (isset($_GET["error"]) AND $_GET["error"]==2) echo '<div class="alert alert-danger" role="alert"><h3>',_('Session timed out! Please, log in again.'),'</h3></div>';
?>
      <form method="POST" action="command.php?action=login">
      <div class="row"><div class="col-lg-12">
            <label for="number" class="control-label"><?php if (issmssystemenabled()==TRUE) echo _('Phone number:'); else echo _('User number:'); ?></label> <input type="text" name="number" id="number" class="form-control" />
       </div></div>
       <div class="row"><div class="col-lg-12">
            <label for="password"><?php echo _('Password:'); ?> <small id="passwordresetblock">(<a id="resetpassword"><?php echo _('Forgotten? Reset password'); ?></a>)</small></label> <input type="password" name="password" id="password" class="form-control" />
       </div></div><br />
       <div class="row"><div class="col-lg-12">
         <button type="submit" id="register" class="btn btn-lg btn-block btn-primary"><?php echo _('Log in'); ?></button>
       </div>
       </div>
         </form>
         <h3 class="center">OR</h3>
         <form action="register.php">
         <div class="row"><div class="col-lg-12">
         <button type="submit" id="register" class="btn btn-lg btn-block btn-primary"><?php echo _('Register'); ?></button>
         </form>
       </div>
</div>
<?php endif; ?>

<?php if(isloggedin()): ?>
  <div id="messageboard"></div>
  <!--Profile edit -->
  <div class="row">
  <form class="container " id="profile">
    <h2 id="profiletitle"><?php echo _('Profile'); ?></h2>
    <h4 id="hint"><?php echo _('Click the save button to save changes'); ?></h4>

    <input type="hidden" name="userid" id="userid"  value="<?php echo $_COOKIE['loguserid']; ?>" />
    <div class="form-group">
        <label for="phonenumber"><?php echo _('Phone number:'); ?></label> <input type="text" name="phonenumber" id="phonenumber" class="form-control" readonly /></div>
    <div class="form-group">
        <label for="username"><?php echo _('Fullname:'); ?></label> <input type="text" name="username" id="username" class="form-control"  /></div>
    <div class="form-group">
        <label for="useremail"><?php echo _('Email:'); ?></label> <input type="text" name="useremail" id="useremail" class="form-control"  /></div>
    <div class="form-group">
        <label for="mailingaddress"><?php echo _('Address(Line 1):'); ?></label> <input type="text" name="mailingaddress" id="mailingaddress" class="form-control"  /></div>
    <div class="form-group">
        <label for="physicaladdress"><?php echo _('Address(Line 2):'); ?></label> <input type="text" name="physicaladdress" id="physicaladdress" class="form-control"  /></div>
    <div class="form-group">
        <label for="city"><?php echo _('City/town:'); ?></label> <input type="text" name="city" id="city" class="form-control"  /></div>
    <div class="form-group">
        <label for="state"><?php echo _('State:'); ?></label> <input type="text" name="state" id="state" class="form-control"  /></div>
    <div class="form-group">
        <label for="zipcode"><?php echo _('zipcode:'); ?></label> <input type="text" name="zipcode" id="zipcode" class="form-control"  /></div>

   <button type="submit" id="saveprofile" class="btn btn-primary" ><?php echo _('Save'); ?></button>
   <button id="closeprofile" class="btn btn-primary" ><?php echo _('Close'); ?></button>
  </form>
  </div>
<?php endif; ?>

<?php if (isloggedin()): ?>
<h2 id="standname"><select id="stands"></select><span id="standcount"></span></h2>
<?php endif; ?>
<div id="standinfo"></div>
<div id="standphoto"></div>
<div id="standbikes"></div>
<div class="row">
   <div class="col-lg-12">
   <div id="console">
   </div>
   </div>
</div>
<div class="row">
<div id="bike" class="col-lg-12">
</br>
  <p></p>
  <img class="bikepic" src="" width="200px" height="200px" />
</div>
</div>
<div class="row">
<div id="standactions" class="btn-group">
  <div class="col-lg-12">
         <button class="btn btn-primary btn-large col-lg-12" type="button" id="rent" title="<?php echo _('Choose bike number and rent bicycle. You will receive a code to unlock the bike and the new code to set.'); ?>"><span class="glyphicon glyphicon-log-out"></span> <?php echo _('Rent'); ?> <span class="bikenumber"></span></button>
  </div>
</div>
</div>
<div class="row"><div class="col-lg-12">
<br /></div></div>
<div id="rentedbikes"></div>
<div class="row">
   <div class="input-group">
   <div class="col-lg-12">
   <input type="text" name="notetext" id="notetext" class="form-control" placeholder="<?php echo _('Describe problem'); ?>">
   </div>
   </div>
</div>
<div class="row">
   <div class="btn-group bicycleactions">
   <div class="col-lg-12">
   <button type="button" class="btn btn-primary" id="return" title="<?php echo _('Return this bicycle to the selected stand.'); ?>"><span class="glyphicon glyphicon-log-in"></span> <?php echo _('Return bicycle'); ?> <span class="bikenumber"></span></button> (<?php echo _('and'); ?> <a href="#" id="note" title="<?php echo _('Use this link to open a text field to write in any issues with the bicycle you are returning (flat tire, chain stuck etc.).'); ?>"><?php echo _('report problem'); ?> <span class="glyphicon glyphicon-exclamation-sign"></span></a>)
   </div></div>
</div>

  <?php if (isloggedin()): ?>
  <div class="row">
  <div class="accordion" id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" id="cbtn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
         <span class="glyphicon glyphicon-film"></span>
          Watch Tutorials
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse"  aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
         
      <div id="videocarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner"></div>
        <a class="carousel-control-prev" href="#videocarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#videocarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        </div>
          <script>videos()</script>
      </div>
    </div>
  </div>
  </div>
  <?php endif; ?>


</div>
</body>
</html>
