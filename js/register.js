$(document).ready(function(){
   addvideolistener();
   populatestates();
   if (window.location.hash)
      {
      hash=window.location.hash.replace("#reset","");
      $('#number').val(hash);
      getsmscode();
      }

   $('#step2').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            smscode: {
                validators: {
                    callback: {
                        message: _sms_code,
                        callback: function(value, validator) {
                                  smscode=$("#smscode").val();
                                  smscode=smscode.replace(/ /g,"");
                                  if (smscode.search('[a-zA-Z]{2}[0-9]{6}')==0) return true;
                                  else return false;
                                  } },
                    notEmpty: {
                        message: _enter_sms_code
                    }
                }
            },
            fullname: {
                validators: {
                    notEmpty: {
                        message: _enter_names
                    }
                }
            },
            mailingaddress: {
                validators: {
                    notEmpty: {
                        message:'Enter mailing address'
                    }
                }
            },
            physicaladdress: {
                validators: {
                    notEmpty: {
                        message: 'Enter physical address'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'Enter city'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'Enter state'
                    }
                }
            },
            zipcode: {
                validators: {
                    notEmpty: {
                        message: 'Enter zipcode'
                    }
                }
            },
            useremail: {
                validators: {
                    emailAddress: {
                        message: _email_incorrect
                    },
                    notEmpty: {
                        message: _enter_email
                    }
                }
            },
            password: {
                validators: {
                    identical: {
                        field: 'password2',
                        message: _passwords_nomatch
                    },
                    notEmpty: {
                        message: _enter_password
                    }
                }
            },
            password2: {
                validators: {
                    identical: {
                        field: 'password',
                        message: _passwords_nomatch
                    },
                    notEmpty: {
                        message: _enter_password
                    }
                }
            }
        }
    }).on('success.form.bv',function(e){ e.preventDefault(); });

   $("#step1").submit(function(e) { e.preventDefault(); getsmscode(); });
   $("#step2").submit(function(e) { e.preventDefault(); register(); });

});

function getsmscode()
{
  
   $.ajax({
   url: "command.php?action=smscode&number="+$('#number').val()
   }).done(function(jsonresponse) {
   jsonobject=$.parseJSON(jsonresponse);
   $('#console').html('');
   $("#validatednumber").val(jsonobject.content);
   $("#checkcode").val(jsonobject.checkcode);
   $("#existing").val(jsonobject.existing);
   $("#step1").fadeOut();
   if (jsonobject.existing==1)
      {
      $('h1').html(_existing_user).fadeIn();
      $('#step2title').html(_step2).fadeIn();
      $('#register').html(_set_password);
      $('#regonly').fadeOut();
      }
   });
}

function register()
{
   $("#console").fadeOut();
   $("#register").prop("disabled", true);
   $.ajax({
   url: "command.php?action=register&validatednumber="+$('#validatednumber').val()+"&checkcode="+$('#checkcode').val()+"&smscode="+$('#smscode').val()+"&fullname="+$('#fullname').val()+"&useremail="+$('#useremail').val()+"&password="+$('#password').val()+"&password2="+$('#password2').val()+"&existing="+$('#existing').val()+"&mailingaddress="+$('#mailingaddress').val()+"&physicaladdress="+$('#physicaladdress').val()+"&city="+$('#city').val()+"&state="+$('#state').val()+"&zipcode="+$('#zipcode').val()
   }).done(function(jsonresponse) {
   jsonobject=$.parseJSON(jsonresponse);
   if (jsonobject.error==1)
      {
      $('#console').html('<div class="alert alert-danger" role="alert">'+jsonobject.content+'</div>');
      $("#console").fadeIn();
      $("#register").prop("disabled", false );
      }
   else
      {
      $("#step2").fadeOut();
      $("#console").fadeIn();
      $('#console').html('<div class="alert alert-success" role="alert">'+jsonobject.content+'</div>');
      }
   });
}

function addvideolistener()
{
    $("#video").bind('ended', function(){
        $.ajax({
            url: "command.php?action=getsystemurl"
            }).done(function(jsonresponse) {
               jsonobject=$.parseJSON(jsonresponse);
               if (jsonobject)
                  {
                    location.href=jsonobject["url"];
                  }
            });  
     }); 
}
function populatestates()
{
    var dropdown = $('#state');

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose State/Province</option>');
    dropdown.prop('selectedIndex', 0);

    var data=[
        {
            "name": "Alabama",
            "abbreviation": "AL"
        },
        {
            "name": "Alaska",
            "abbreviation": "AK"
        },
        {
            "name": "American Samoa",
            "abbreviation": "AS"
        },
        {
            "name": "Arizona",
            "abbreviation": "AZ"
        },
        {
            "name": "Arkansas",
            "abbreviation": "AR"
        },
        {
            "name": "California",
            "abbreviation": "CA"
        },
        {
            "name": "Colorado",
            "abbreviation": "CO"
        },
        {
            "name": "Connecticut",
            "abbreviation": "CT"
        },
        {
            "name": "Delaware",
            "abbreviation": "DE"
        },
        {
            "name": "District Of Columbia",
            "abbreviation": "DC"
        },
        {
            "name": "Federated States Of Micronesia",
            "abbreviation": "FM"
        },
        {
            "name": "Florida",
            "abbreviation": "FL"
        },
        {
            "name": "Georgia",
            "abbreviation": "GA"
        },
        {
            "name": "Guam",
            "abbreviation": "GU"
        },
        {
            "name": "Hawaii",
            "abbreviation": "HI"
        },
        {
            "name": "Idaho",
            "abbreviation": "ID"
        },
        {
            "name": "Illinois",
            "abbreviation": "IL"
        },
        {
            "name": "Indiana",
            "abbreviation": "IN"
        },
        {
            "name": "Iowa",
            "abbreviation": "IA"
        },
        {
            "name": "Kansas",
            "abbreviation": "KS"
        },
        {
            "name": "Kentucky",
            "abbreviation": "KY"
        },
        {
            "name": "Louisiana",
            "abbreviation": "LA"
        },
        {
            "name": "Maine",
            "abbreviation": "ME"
        },
        {
            "name": "Marshall Islands",
            "abbreviation": "MH"
        },
        {
            "name": "Maryland",
            "abbreviation": "MD"
        },
        {
            "name": "Massachusetts",
            "abbreviation": "MA"
        },
        {
            "name": "Michigan",
            "abbreviation": "MI"
        },
        {
            "name": "Minnesota",
            "abbreviation": "MN"
        },
        {
            "name": "Mississippi",
            "abbreviation": "MS"
        },
        {
            "name": "Missouri",
            "abbreviation": "MO"
        },
        {
            "name": "Montana",
            "abbreviation": "MT"
        },
        {
            "name": "Nebraska",
            "abbreviation": "NE"
        },
        {
            "name": "Nevada",
            "abbreviation": "NV"
        },
        {
            "name": "New Hampshire",
            "abbreviation": "NH"
        },
        {
            "name": "New Jersey",
            "abbreviation": "NJ"
        },
        {
            "name": "New Mexico",
            "abbreviation": "NM"
        },
        {
            "name": "New York",
            "abbreviation": "NY"
        },
        {
            "name": "North Carolina",
            "abbreviation": "NC"
        },
        {
            "name": "North Dakota",
            "abbreviation": "ND"
        },
        {
            "name": "Northern Mariana Islands",
            "abbreviation": "MP"
        },
        {
            "name": "Ohio",
            "abbreviation": "OH"
        },
        {
            "name": "Oklahoma",
            "abbreviation": "OK"
        },
        {
            "name": "Oregon",
            "abbreviation": "OR"
        },
        {
            "name": "Palau",
            "abbreviation": "PW"
        },
        {
            "name": "Pennsylvania",
            "abbreviation": "PA"
        },
        {
            "name": "Puerto Rico",
            "abbreviation": "PR"
        },
        {
            "name": "Rhode Island",
            "abbreviation": "RI"
        },
        {
            "name": "South Carolina",
            "abbreviation": "SC"
        },
        {
            "name": "South Dakota",
            "abbreviation": "SD"
        },
        {
            "name": "Tennessee",
            "abbreviation": "TN"
        },
        {
            "name": "Texas",
            "abbreviation": "TX"
        },
        {
            "name": "Utah",
            "abbreviation": "UT"
        },
        {
            "name": "Vermont",
            "abbreviation": "VT"
        },
        {
            "name": "Virgin Islands",
            "abbreviation": "VI"
        },
        {
            "name": "Virginia",
            "abbreviation": "VA"
        },
        {
            "name": "Washington",
            "abbreviation": "WA"
        },
        {
            "name": "West Virginia",
            "abbreviation": "WV"
        },
        {
            "name": "Wisconsin",
            "abbreviation": "WI"
        },
        {
            "name": "Wyoming",
            "abbreviation": "WY"
        }
    ];

    // Populate dropdown with list of provinces
    $.each(data, function (key, entry) {
        dropdown.append($('<option></option>').attr('value', entry.abbreviation).text(entry.name));
    });
    
}

