<?php
require("config.php");
require("db.class.php");
require('actions-web.php');

$db=new Database($dbserver,$dbuser,$dbpassword,$dbname);
$db->connect();

if (isset($_COOKIE["loguserid"])) $userid=$_COOKIE["loguserid"];
else $userid=0;
if (isset($_COOKIE["logsession"])) $session=$_COOKIE["logsession"];
$action="";
if (isset($_GET["action"])) $action=trim($_GET["action"]);

switch($action)
   {
   case "smscode":
      $number=trim($_GET["number"]);
      smscode($number);
      break;
   case "register":
      $number=trim($_GET["validatednumber"]);
      $smscode=trim($_GET["smscode"]);
      $checkcode=trim($_GET["checkcode"]);
      $fullname=trim($_GET["fullname"]);
      $useremail=trim($_GET["useremail"]);
      $password=trim($_GET["password"]);
      $password2=trim($_GET["password2"]);
      $existing=trim($_GET["existing"]);
      $mailingaddress=trim($_GET["mailingaddress"]);
      $physicaladdress=trim($_GET["physicaladdress"]);
      $city=trim($_GET["city"]);
      $state=trim($_GET["state"]);
      $zipcode=trim($_GET["zipcode"]);
      register($number,$smscode,$checkcode,$fullname,$useremail,$password,$password2,$existing,$mailingaddress,$physicaladdress,$city,$state,$zipcode);
      break;
   case "login":
      $number=trim($_POST["number"]);
      $password=trim($_POST["password"]);
      login($number,$password);
      break;
   case "logout":
      logout();
      break;
   case "resetpassword":
      resetpassword($_GET["number"]);
      break;
   case "list":
      $stand=trim($_GET["stand"]);
      listbikes($stand);
      break;
   case "rent":
      logrequest($userid,$action);
      checksession();
      $bikeno=trim($_GET["bikeno"]);
      checkbikeno($bikeno);
      rent($userid,$bikeno);
      break;
   case "return":
      logrequest($userid,$action);
      checksession();
      $bikeno=trim($_GET["bikeno"]);
      $stand=trim($_GET["stand"]);
      $note="";
      if (isset($_GET["note"])) $note=trim($_GET["note"]);
      checkbikeno($bikeno); checkstandname($stand);
      returnBike($userid,$bikeno,$stand,$note);
      break;
   case "validatecoupon":
      logrequest($userid,$action);
      checksession();
      $coupon=trim($_GET["coupon"]);
      validatecoupon($userid,$coupon);
      break;
   case "forcerent":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      $bikeno=trim($_GET["bikeno"]);
      checkbikeno($bikeno);
      rent($userid,$bikeno,TRUE);
      break;
   case "forcereturn":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      $bikeno=trim($_GET["bikeno"]);
      $stand=trim($_GET["stand"]);
      $note="";
      if (isset($_GET["note"])) $note=trim($_GET["note"]);
      checkbikeno($bikeno); checkstandname($stand);
      returnBike($userid,$bikeno,$stand,$note,TRUE);
      break;
   case "where":
      logrequest($userid,$action);
      checksession();
      $bikeno=trim($_GET["bikeno"]);
      checkbikeno($bikeno);
      where($userid,$bikeno);
      break;
   case "removenote":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      checkbikeno($bikeno);
      removenote($userid,$bikeno);
      break;
   case "revert":
      logrequest($userid,$action);
      checksession();
      $bikeno=trim($_GET["bikeno"]);
      checkprivileges($userid);
      checkbikeno($bikeno);
      revert($userid,$bikeno);
      break;
   case "last":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      if ($_GET["bikeno"])
         {
         $bikeno=trim($_GET["bikeno"]);
         checkbikeno($bikeno);
         last($userid,$bikeno);
         }
      else last($userid);
      break;
   case "stands":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      liststands();
      break;
   case "standsmin":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      liststandsmin();
      break;
  case "braintree":
      braintree();
      break;
   case "userlist":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      getuserlist();
      break;
  case "videolist":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      getvideolist();
      break;
 case "inquirylist":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      getinquirylist();
      break;
   case "userstats":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      getuserstats();
      break;
   case "usagestats":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      getusagestats();
      break;
   case "edituser":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      edituser($_GET["edituserid"]);
      break;
 case "editinquiry":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      editinquiry($_GET["inquiryid"]);
      break;
  case "editprofile":
      logrequest($userid,$action);
      checksession();
      edituser($_GET["edituserid"]);
      break;
    case "editvideo":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      editvideo($_GET["editvideoid"]);
      break;
   case "editbicycle":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      editbicycle($_GET["editbicycleid"]);
      break;
  case "getbicyclephoto":
      logrequest($userid,$action);
      checksession();
      getbicyclephoto($_GET["bicycleid"]);
      break;
  case "getsystemurl":
      getsystemurl();
      break;
   case "editstand":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      editstand($_GET["editstandid"]);
      break;
   case "saveuser":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      saveuser($_GET["edituserid"],$_GET["username"],$_GET["email"],$_GET["mailingaddress"],$_GET["physicaladdress"],$_GET["city"],$_GET["state"],$_GET["zipcode"],$_GET["phone"],$_GET["privileges"],$_GET["limit"]);
      break;
  case "saveprofile":
      logrequest($userid,$action);
      checksession();
      saveprofile($_GET["edituserid"],$_GET["username"],$_GET["email"],$_GET["mailingaddress"],$_GET["physicaladdress"],$_GET["city"],$_GET["state"],$_GET["zipcode"]);
      break;
  case "saveinquiry":
      saveinquiry($_GET["userid"],$_GET["phone"],$_GET["email"],$_GET["inquiry"]);
      break;
  case "message":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      message($_GET["message"]);
      break;
   case "savebicycle":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      savebicycle($_POST["editbicycleid"],$_POST["currentstand"],$_POST["file"],$_POST["note"]);
      break;
   case "savestand":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      savestand($_POST["editstandid"],$_POST["standname"],$_POST["description"],$_POST["placename"],$_POST["active"],$_POST["longitude"],$_POST["latitude"],$_POST["file"]);
      break;
    case "addnewstand":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      addnewstand($_POST["standname"],$_POST["description"],$_POST["placename"],$_POST["longitude"],$_POST["latitude"],$_POST["file"]);
      break;
    case "addnewbicycle":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      addnewbicycle($_POST["currentstand"],$_POST["file"]);
      break;
    case "addnewvideo":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      addnewvideo($_POST["filename"],$_POST["file"],$_POST["thumbnail"]);
      break;
    case "deletebicycle":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      deletebicycle($_GET["deleteid"]);
      break;
    case "closeinquiry":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      closeinquiry($_GET["inquiryid"]);
      break;
    case "deletestand":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      deletestand($_GET["deleteid"]);
      break;
    case "deletevideo":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      deletevideo($_GET["deleteid"]);
      break;
   case "addcredit":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      addcredit($_GET["edituserid"],$_GET["creditmultiplier"]);
      break;
   case "trips":
      logrequest($userid,$action);
      checksession();
      checkprivileges($userid);
      if ($_GET["bikeno"])
         {
         $bikeno=trim($_GET["bikeno"]);
         checkbikeno($bikeno);
         trips($userid,$bikeno);
         }
      else trips($userid);
      break;
   case "userbikes":
      userbikes($userid);
      break;
   case "couponlist":
      logrequest($userid,$action);
      checksession();
      getcouponlist();
      break;
   case "generatecoupons":
      logrequest($userid,$action);
      checksession();
      generatecoupons($_GET["multiplier"]);
      break;
   case "sellcoupon":
      logrequest($userid,$action);
      checksession();
      sellcoupon($_GET["coupon"]);
      break;
   case "map:standmarkers":
      mapgetstandmarkers();
      break;
   case "map:markers":
      mapgetmarkers();
      break;
   case "map:status":
      mapgetlimit($userid);
      break;
   case "map:geolocation":
      $lat=floatval(trim($_GET["lat"]));
      $long=floatval(trim($_GET["long"]));
      mapgeolocation($userid,$lat,$long);
      break;
   }

?>