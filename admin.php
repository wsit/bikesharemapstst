<?php
require("config.php");
require("db.class.php");
require('actions-web.php');

$db=new Database($dbserver,$dbuser,$dbpassword,$dbname);
$db->connect();

checksession();
if (getprivileges($_COOKIE["loguserid"])<=0) exit(_('You need admin privileges to access this page.'));
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title><?php echo $systemname; ?> <?php echo _('administration'); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="js/translations.php"></script>
<script type="text/javascript" src="js/admin.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrapValidator.min.css" />
<?php if (file_exists("analytics.php")) require("analytics.php"); ?>
<script>
<?php
if (iscreditenabled())
   {
   echo 'var creditenabled=1;',"\n";
   echo 'var creditcurrency="',$credit["currency"],'"',";\n";
   $requiredcredit=$credit["min"]+$credit["rent"]+$credit["longrental"];
   }
else
   {
   echo 'var creditenabled=0;',"\n";
   }
?>
</script>
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only"><?php echo _('Toggle navigation'); ?></span>
          </button>
          <a class="navbar-brand" href="<?php echo $systemURL; ?>"><?php echo $systemname; ?></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo $systemURL; ?>"><?php echo _('Map'); ?></a></li>
            <li class="active"><a href="<?php echo $systemURL; ?>admin.php"><?php echo _('Admin'); ?></a></li>
<?php if (isloggedin()): ?>
            <li><a href="command.php?action=logout" id="logout"><?php echo _('Log out'); ?></a></li>
<?php endif; ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
<br />
    <div class="container">

      <div class="page-header">
            <h1><?php echo _('Administration'); ?></h1>
            </div>

<?php
if (isloggedin()):
?>
            <div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#fleet" aria-controls="fleet" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> <?php echo _('Fleet'); ?></a></li>
    <li role="presentation"><a href="#stands" aria-controls="stands" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo _('Stands'); ?></a></li>
    <li role="presentation"><a href="#users" aria-controls="users" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo _('Users'); ?></a></li>
    <li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-film" aria-hidden="true"></span> <?php echo _('Videos'); ?></a></li>
    <li role="presentation"><a href="#inquiries" aria-controls="inquiries" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> <?php echo _('Inquiries'); ?></a></li>
<?php
if (iscreditenabled()):
?>
    <li role="presentation"><a href="#credit" aria-controls="credit" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> <?php echo _('Credit system'); ?></a></li>
<?php endif; ?>
    <li role="presentation"><a href="#reports" aria-controls="reports" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> <?php echo _('Reports'); ?></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="fleet">
      <div class="row">
      <div class="col-lg-12">
        <input type="text" name="adminparam" id="adminparam" class="form-control">
        <button class="btn btn-default" type="button" id="where" title="<?php echo _('Display the bike stand location or name of person using it.'); ?>"><span class="glyphicon glyphicon-screenshot"></span> <?php echo _('Where is?'); ?></button>
         <button type="button" id="revert" class="btn btn-default" title="<?php echo _('Be careful! Revert accidentaly rented bike in case of mistake or misread bike number.'); ?>"><span class="glyphicon glyphicon-fast-backward"></span> <?php echo _('Revert'); ?></button>
         <button type="button" id="last" class="btn btn-default" title="<?php echo _('Display network usage (blank) or history of bike usage (number entered).'); ?>"><span class="glyphicon glyphicon-stats"></span> <?php echo _('Last usage'); ?></button>
         
         <div id="fleetconsole"></div>
         </div>
      </div>

    </div>
    <div role="tabpanel" class="tab-pane" id="stands">
      <div class="row">
         <div class="col-lg-12">
          <button type="button" id="stands" class="btn btn-default" title="Refresh stand list."><span class="glyphicon glyphicon-repeat"></span> <?php echo _(''); ?></button>
          <button type="button" id="newstand" class="btn btn-default" title="Add new stand."><span class="glyphicon glyphicon-plus"></span> <?php echo _('Add stand'); ?></button>
          <button type="button" id="newbicycle" class="btn btn-default" title="Add new bike."><span class="glyphicon glyphicon-plus"></span> <?php echo _('Add bike'); ?></button>
          
         </div>
      </div>
      <form class="container" id="addstand" enctype="multipart/form-data">
        <div><h3><?php echo _('New Stand') ?></h3></div>
        
        <div class="form-group"><label for="standname" class="control-label"><?php echo _('Stand name:'); ?></label> <input type="text" name="standname" id="standname" class="form-control" /></div>
        <div class="form-group"><label for="description" class="control-label"><?php echo _('Description:'); ?></label> <input type="text" name="description" id="description" class="form-control" /></div>
        <div class="form-group"><label for="placename" class="control-label"><?php echo _('Place:'); ?></label> <input type="text" name="placename" id="placename" class="form-control" /></div>
        <div class="form-group">
        <label for="longitude" class="control-label"><?php echo _('Longitude:'); ?></label> <input type="text" name="longitude" id="longitude" class="form-control" />
        <label for="latitude" class="control-label"><?php echo _('Latitude:'); ?></label> <input type="text" name="latitude" id="latitude" class="form-control" />
        </div>
        <div class="form-group"><label for="file" class="control-label"><?php echo _('Picture:'); ?></label> <input type="file" name="file" id="file" /></div>
        </br>
        <button type="button" id="savenewstand" class="btn btn-primary"><?php echo _('Save'); ?></button>
        </br>
      </form>
      <form class="container" id="editstand" enctype="multipart/form-data">
        <div><h3><?php echo _('Edit Stand') ?></h3></div>
        <input type="hidden" name="standid" id="standid" />
        <div class="form-group"><label for="standname" class="control-label"><?php echo _('Stand name:'); ?></label> <input type="text" name="standname" id="standname" class="form-control" /></div>
        <div class="form-group"><label for="description" class="control-label"><?php echo _('Description:'); ?></label> <input type="text" name="description" id="description" class="form-control" /></div>
        <div class="form-group"><label for="placename" class="control-label"><?php echo _('Place:'); ?></label> <input type="text" name="placename" id="placename" class="form-control" /></div>
        <div class="form-group"><label for="active" class="control-label"><?php echo _('Active:'); ?></label> <input type="text" name="active" id="active" class="form-control" /></div>
        <div class="form-group">
        <label for="longitude" class="control-label"><?php echo _('Longitude:'); ?></label> <input type="text" name="longitude" id="longitude" class="form-control" />
        <label for="latitude" class="control-label"><?php echo _('Latitude:'); ?></label> <input type="text" name="latitude" id="latitude" class="form-control" />
        </div>
        <div class="form-group"><label for="standphoto" class="control-label"><?php echo _('Picture:'); ?></label> <input type="file" name="file" id="file" /></div>
        <div><img id="fileview" src="" class="rounded mx-auto d-block" width="200px" height="200px" /></div>
        </br>
        <button type="button" id="savestand" class="btn btn-primary"><?php echo _('Save'); ?></button>
        <button type="button" id="deletestand" class="btn btn-danger"><?php echo _('Decomission'); ?></button>
        </br>
      </form>
      <form class="container" id="addbicycle" enctype="multipart/form-data">
        <div><h3><?php echo _('New Bike') ?></h3></div>
        <div class="form-group"><label for="currentstand" class="control-label"><?php echo _('Stand:'); ?></label> <select name="currentstand" id="currentstand" class="form-control"></select></div>
        
        <div class="form-group"><label for="file"><?php echo _('Picture:'); ?></label> <input type="file" name="file" id="file"  /></div>
        <button type="button" id="savenewbicycle" class="btn btn-primary"><?php echo _('Save'); ?></button>
        </br>
      </form>
      <form class="container" id="editbicycle" enctype="multipart/form-data">
        <div><h3><?php echo _('Edit Bicycle') ?></h3></div>
        <div class="form-group"><label for="currentstand" class="control-label"><?php echo _('Stand:'); ?></label> <select name="currentstand" id="currentstand" class="form-control"></select></div>
        <div><img id="fileview" src="" class="rounded mx-auto d-block" width="200px" height="200px" /></div>
        <div class="form-group"><label for="file"><?php echo _('Picture:'); ?></label> <input type="file" name="file" id="file"  /></div>
        <div class="form-group"><label for="note"><?php echo _('Notes:'); ?></label> <textarea rows="4" cols="40" name="note" id="note" class="form-control"></textarea></div>
        <input type="hidden" name="bicycleid" id="bicycleid" />
        <button type="button" id="savebicycle" class="btn btn-primary"><?php echo _('Save'); ?></button>
        <button type="button" id="deletebicycle" class="btn btn-danger"><?php echo _('Decomission'); ?></button>
        </br>
      </form>
      </br>
      <div id="standsconsole"></div>
    </div>
    
<?php
if (iscreditenabled()):
?>
    <div role="tabpanel" class="tab-pane" id="credit">
      <div class="row">
         <div class="col-lg-12">
         <button type="button" id="listcoupons" class="btn btn-default" title="<?php echo _('Display existing coupons.'); ?>"><span class="glyphicon glyphicon-list-alt"></span> <?php echo _('List coupons'); ?></button>
         <button type="button" id="generatecoupons1" class="btn btn-success" title="<?php echo _('Generate new coupons.'); ?>"><span class="glyphicon glyphicon-plus"></span> <?php echo _('Generate'); echo ' ',$requiredcredit,$credit["currency"],' '; echo _('coupons'); ?></button>
         <button type="button" id="generatecoupons2" class="btn btn-success" title="<?php echo _('Generate new coupons.'); ?>"><span class="glyphicon glyphicon-plus"></span> <?php echo _('Generate'); echo ' ',$requiredcredit*5,$credit["currency"],' '; echo _('coupons'); ?></button>
         <button type="button" id="generatecoupons3" class="btn btn-success" title="<?php echo _('Generate new coupons.'); ?>"><span class="glyphicon glyphicon-plus"></span> <?php echo _('Generate'); echo ' ',$requiredcredit*10,$credit["currency"],' '; echo _('coupons'); ?></button>
         </br>
         <div id="creditconsole"></div>
         </div>
      </div>
    </div>
<?php endif; ?>
    <div role="tabpanel" class="tab-pane" id="users">
      <div class="row">
         <div class="col-lg-12">
         <button type="button" id="userlist" class="btn btn-default" title="<?php echo _('Refresh list of users.'); ?>"><span class="glyphicon glyphicon-repeat"></span> <?php echo _(''); ?></button>
         <button type="button" id="newmessage" class="btn btn-default" title="<?php echo _('Broadcast message'); ?>"><span class="glyphicon glyphicon-chat"></span> <?php echo _('Broadcast message'); ?></button>
         </div>
      </div>
      <form class="container" id="edituser">
         <div><h3><?php echo _('Edit User') ?></h3></div>
         <div class="form-group"><label for="username" class="control-label"><?php echo _('Fullname:'); ?></label> <input type="text" name="username" id="username" class="form-control" /></div>
         <div class="form-group"><label for="email"><?php echo _('Email:'); ?></label> <input type="text" name="email" id="email" class="form-control" /></div>
<?php //if ($connectors["sms"]): ?>
         <div class="form-group"><label for="phone"><?php echo _('Phone number:'); ?></label> <input type="text" name="phone" id="phone" class="form-control" /></div>
<?// endif; ?>
        <div class="form-group"><label for="mailingaddress"><?php echo _('Address(Line 1):'); ?></label> <input type="text" name="mailingaddress" id="mailingaddress" class="form-control" /></div>
        <div class="form-group"><label for="physicaladdress"><?php echo _('Address(Line 2):'); ?></label> <input type="text" name="physicaladdress" id="physicaladdress" class="form-control" /></div>
        <div class="form-group"><label for="city"><?php echo _('City/Town:'); ?></label> <input type="text" name="city" id="city" class="form-control" /></div>
        <div class="form-group"><label for="state"><?php echo _('State:'); ?></label> <input type="text" name="state" id="state" class="form-control" /></div>
        <div class="form-group"><label for="zipcode"><?php echo _('ZIP/ Postal Code:'); ?></label> <input type="text" name="zipcode" id="zipcode" class="form-control" /></div>
         <div class="form-group"><label for="privileges"><?php echo _('Privileges:'); ?></label> <input type="text" name="privileges" id="privileges" class="form-control" /></div>
         <div class="form-group"><label for="limit"><?php echo _('Bike limit:'); ?></label> <input type="text" name="limit" id="limit" class="form-control" /></div>
         <input type="hidden" name="userid" id="userid" value="" />
         <button type="button" id="saveuser" class="btn btn-primary"><?php echo _('Save'); ?></button>
         or <button type="button" id="addcredit" class="btn btn-success"><?php echo _('Add'); echo ' ',$requiredcredit,$credit["currency"]; ?></button>
         <button type="button" id="addcredit2" class="btn btn-success"><?php echo _('Add'); echo ' ',$requiredcredit*5,$credit["currency"]; ?></button>
         <button type="button" id="addcredit3" class="btn btn-success"><?php echo _('Add'); echo ' ',$requiredcredit*10,$credit["currency"]; ?></button>
         </br>
      </form>
      <form class="container" id="broadcast">
        <div><h3><?php echo _('Broadcast message') ?></h3></div>
        <div class="form-group"><label for="message"><?php echo _('Message:'); ?></label> 
        <textarea type="text" name="message" id="message" class="form-control"></textarea></div>
        <button type="button" id="send" class="btn btn-primary"><?php echo _('Send'); ?></button>
      </form>
      </br>
      <div id="userconsole"></div>
    </div>
    <div role="tabpanel" class="tab-pane" id="videos">
      <div class="row">
        <div class="col-lg-12">
        <button type="button" id="videolist" class="btn btn-default" title=""><span class="glyphicon glyphicon-repeat"></span></button>
        <button type="button" id="newvideo" class="btn btn-default" title=""><span class="glyphicon glyphicon-film"></span> <?php echo _('Add Video'); ?></button>
        </div>
      </div>
      <div class="progress">
        <p>Uploading...</p>
      </div>
      <form class="container" id="editvideo">
        <div><h3><?php echo _('Edit Video') ?></h3></div>
        <input type="hidden" name="videoid" id="videoid" />
        <div class="form-group"><label for="filename"><?php echo _('Filename:'); ?></label> <input type="text" name="filename" id="filename" class="form-control" /></div>
        <div class="form-group"><label for="file"><?php echo _('Video:'); ?></label> <input type="file" name="file" id="file"  /></div>
        <div class="form-group"><label for="thumbnail"><?php echo _('Thumbnail:'); ?></label> <input type="file" name="thumbnail" id="thumbnail"  /></div>
        <table class="table table-striped">
          <thead>
          <tr>  <th>Video</th> </tr>
          </thead>
          <tbody>
            <tr> <td> <video name="vvideoview" id="vvideoview" src="" width="200" height="200" controls>Your device does not support video</video> </td> </tr>
          </tbody>
        </table>
        
        <button type="button" id="deletevideo" class="btn btn-danger"><?php echo _('Delete'); ?></button>
      </form>
      <form class="container" id="addvideo">
        <div><h3><?php echo _('New Video') ?></h3></div>
        <div class="form-group"><label for="filename"><?php echo _('Filename:'); ?></label> <input type="text" name="filename" id="filename" class="form-control" /></div>
        <div class="form-group"><label for="file"><?php echo _('Video: (10 MB Max)'); ?></label> <input type="file" name="file" id="file"  /></div>
        <div class="form-group"><label for="thumbnail"><?php echo _('Thumbnail:'); ?></label> <input type="file" name="thumbnail" id="thumbnail"  /></div>
        <button type="button" id="savenewvideo" class="btn btn-primary"><?php echo _('Save'); ?></button>
      </form>
      </br>
      <div id="videoconsole"></div>
    </div>

    <div role="tabpanel" class="tab-pane" id="inquiries">
      <div class="row">
        <div class="col-lg-12">
        <button type="button" id="reloadinquiries" class="btn btn-default" title="<?php echo _('Reload inquiries.'); ?>"><span class="glyphicon glyphicon-repeat"></span> <?php ?></button>
        <form class="container" id="editinquiry">
          <h2 ><?php echo _('Inquiry review'); ?></h2>
          <div class="form-group">
              <textarea  rows="5" name="inquiry" id="inquiry" class="form-control"> </textarea> </div>
          <input type="hidden" name="userid" id="userid" />
          <input type="hidden" name="inquiryid" id="inquiryid" />
          <div class="form-group">
              <label for="phonenumber"><?php echo _('Phone number:'); ?></label> <input type="text" name="phonenumber" id="phonenumber" class="form-control"  /></div>
          <div class="form-group">
              <label for="useremail"><?php echo _('Email:'); ?></label> <input type="text" name="useremail" id="useremail" class="form-control"  /></div>
          <div class="form-group">
              <label for="solved"><?php echo _('Solved:'); ?></label> <input type="text" name="solved" id="solved" class="form-control" style="width: 25%;"  readonly /></div>
          
        <button type="submit" id="closeinquiry" class="btn btn-primary" ><?php echo _('Close inquiry'); ?></button>
      </form>
         </br>
        <div id="inquiryconsole"></div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="reports">
      <div class="row">
         <div class="col-lg-12">
         <button type="button" id="usagestats" class="btn btn-default" title="<?php echo _('Show usage stats by day.'); ?>"><span class="glyphicon glyphicon-road"></span> <?php echo _('Daily stats'); ?></button>
         <button type="button" id="userstats" class="btn btn-default" title="<?php echo _('Show user stats.'); ?>"><span class="glyphicon glyphicon-road"></span> <?php echo _('User stats'); ?></button>
         <!-- button type="button" id="trips" class="btn btn-default" title="<?php echo _('Show history of stand to stand bike trips as lines.'); ?>"><span class="glyphicon glyphicon-road"></span> <?php echo _('Trips overlay'); ?></button -->
         <div id="reportsconsole"></div>
         </div>
      </div>
    </div>
  </div>

   </div>

<?php endif; ?>

            <br />
   <div class="panel panel-default">
  <div class="panel-body">
    <i class="glyphicon glyphicon-copyright-mark"></i> <? echo date("Y"); ?> <a href="<?php echo $systemURL; ?>"><?php echo $systemname; ?></a>
  </div>
  <div class="panel-footer"><strong><?php echo _('Privacy policy:'); ?></strong> <?php echo _('We will use your details for'); echo $systemname,'-'; echo _('related activities only'); ?>.</div>
   </div>

    </div><!-- /.container -->
</body>
</html>
